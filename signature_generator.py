from tkinter import *
import os
import platform
import subprocess

def open_file(path):
    if platform.system() == "Windows":
        os.startfile(path)
    elif platform.system() == "Darwin":
        subprocess.Popen(["open", path])
    else:
        subprocess.Popen(["xdg-open", path])

seperator = '<span style="display: inline; font-family: Arial, &quot;Helvetica Neue&quot;, Helvetica, sans-serif;"> | </span>'


def generate_signature(name, job_title, email, direct_dial, mobile, linkedin):
    linkedin_block = '<a style="text-decoration: none; display: inline;" href="LINKEDINURL"><img width="24" style="margin-bottom:2px; border:none; display:inline;" height="24" data-filename="linkedin.png" src="https://s3.amazonaws.com/htmlsig-assets/polygon/linkedin.png" alt="LinkedIn"></a>'
    
    template = open('input/template.html', 'r', encoding='utf-8')
    code = template.read()
    code = code.replace('{{name}}', name)
    
    if job_title:
        code = code.replace('{{job_title}}', job_title)
        code = code.replace('{{name_title_seperator}}', seperator)
    elif not job_title:
        code = code.replace('{{job_title}}', '')
        code = code.replace('{{name_title_seperator}}', '')
    
    if direct_dial:
        code = code.replace('{{direct_dial}}', f'DD: {direct_dial}')
        code = code.replace('{{direct_dial_seperator}}', seperator)
    elif not direct_dial:
        code = code.replace('{{direct_dial}}', '')
        code = code.replace('{{direct_dial_seperator}}', '')

    if linkedin:
        linkedin_block = linkedin_block.replace('LINKEDINURL', linkedin)
        code = code.replace('{{linkedin}}', linkedin_block)
    elif not linkedin:
        code = code.replace('{{linkedin}}', '')


    code = code.replace('{{email}}', email)
    code = code.replace('{{direct_dial}}', direct_dial)
    code = code.replace('{{mobile}}', f'M: {mobile}')
    path = f"output/{''.join(name.split())}-signature.html"
    

    with open(path, "w+") as f:
        f.write(code)

    print(f'Generated {path}')
    open_file(path)
    return path


def show_entry_fields():
   path = generate_signature(name.get(), job_title.get(), email.get(), direct_dial.get(), mobile.get(), linkedin.get())
   label = Label(master, text=f'Generated {path}')
#    label.pack()
   name.delete(0,END)
   job_title.delete(0,END)
   email.delete(0,END)
   direct_dial.delete(0,END)
   mobile.delete(0,END)
   linkedin.delete(0, END)

master = Tk()
master.title('Signature Generator')
Label(master, text="Name").grid(row=0)
Label(master, text="Job Title").grid(row=1)
Label(master, text="Email").grid(row=2)
Label(master, text="Direct Dial").grid(row=3)
Label(master, text="Mobile").grid(row=4)
Label(master, text="LinkedIn").grid(row=5)

name = Entry(master) 
job_title = Entry(master) 
email = Entry(master) 
direct_dial = Entry(master)
mobile = Entry(master)
linkedin = Entry(master)

name.grid(row=0, column=1)
job_title.grid(row=1, column=1)
email.grid(row=2,column=1)
direct_dial.grid(row=3,column=1)
mobile.grid(row=4,column=1)
linkedin.grid(row=5,column=1)

Button(master, text='Quit', command=master.quit).grid(row=6, column=0, sticky=W, pady=4)
Button(master, text='Generate', command=show_entry_fields).grid(row=6, column=1, sticky=W, pady=4)

mainloop()